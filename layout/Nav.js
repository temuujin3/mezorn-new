import Image from "next/image";
import { NavstickIcon } from "../assets/icons";
import { HireCard } from "../components/HireCard";
import { useTranslation } from "next-i18next";
import { gsap } from "gsap";
import { AnimatedNavButton } from "../components/AnimatedNavButton";
import { useEffect, useRef } from "react";
import LanguageToggle from "../components/LanguageToggle";
import Link from "next/link";
import { Router, useRouter } from "next/router";

export const Nav = ({ toggleMenu, isOpen }) => {
  const { t } = useTranslation();
  const card = useRef();
  const router = useRouter();

  useEffect(() => {
    const items = gsap.utils.selector(card.current);

    if (isOpen) {
      gsap.to(items("#hirecard"), {
        x: 50,
        opacity: 0,
        duration: 0.3,
      });
      gsap.to(items("#languageswitch"), {
        x: 0,
        opacity: 1,
        duration: 0.5,
        delay: 0.3,
      });
    } else {
      gsap.to(items("#hirecard"), {
        x: 0,
        opacity: 1,
        duration: 0.5,
      });
      gsap.to(items("#languageswitch"), {
        x: -20,
        opacity: 0,
        duration: 0.5,
      });
    }

    return () => {};
  }, [isOpen]);

  return (
    <nav className="fixed inset-x-0 top-0 z-50 flex flex-row justify-between p-5 ">
      {router.pathname == "/" ? (
        <Image
          src={"/symbol.svg"}
          width={45}
          height={45}
          className="cursor-pointer"
        />
      ) : (
        <Link href="/">
          <Image
            src={"/symbol.svg"}
            width={45}
            height={45}
            className="cursor-pointer"
          />
        </Link>
      )}

      <div ref={card} className="flex flex-row items-center space-x-2 sm:space-x-5">
        <div id="hirecard">
          <HireCard text={t("common:hire-card")} />
        </div>
        <div id="languageswitch">{isOpen && <LanguageToggle />}</div>
        <div onClick={toggleMenu} className="cursor-pointer">
          {/* <Image src={NavstickIcon} /> */}
          <AnimatedNavButton isOpen={isOpen} />
        </div>
      </div>
    </nav>
  );
};
