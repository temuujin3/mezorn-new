export const MainLayout = ({children}) => {
    return (
        <main className="grid w-full grid-cols-12">
            {children}
        </main>
    );
}
