import { useEffect, useState } from "react";

export const useTranslate = () => {
  const [translateDistance, setTranslateDistance] = useState(0);

  useEffect(() => {
    let prevScroll = 0;

    const updatePosition = () => {
      var st = window.pageYOffset || document.documentElement.scrollTop;
      // if (st > prevScroll) {
      //   // scroll down 
      //   console.log("down")
      // } else {
      //   console.log("up")
      // }

      setTranslateDistance("translateX(" + window.pageYOffset + "px)");

      prevScroll = st <= 0 ? 0 : st; // For Mobile or negative scrolling
    };

    window.addEventListener("scroll", updatePosition);

    return () => {
      window.removeEventListener("scroll", updatePosition);
    };
  });

  return translateDistance;
};
