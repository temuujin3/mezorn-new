import ArrowRightIcon from "./arrow-right.svg";
import AwsIcon from "./aws.svg";
import CssIcon from "./css.svg";
import DialIcon from "./dial.svg";
import FacebookIcon from "./facebook.svg";
import FlutterIcon from "./flutter.svg";
import HtmlIcon from "./html.svg";
import InstagramIcon from "./instagram.svg";
import JavascriptIcon from "./javascript.svg";
import KotlinIcon from "./kotlin.svg";
import LinkIcon from "./link.svg";
import MapIcon from "./map.svg";
import MongodbIcon from "./mongodb.svg";
import NavstickIcon from "./navstick.svg";
import NavstickCloseIcon from "./navstickclose.svg";
import NodejsIcon from "./nodejs.svg";
import ReactIcon from "./react.svg";
import SendIcon from "./send.svg";
import SwiftIcon from "./swift.svg";
import TwitterIcon from "./twitter.svg";
import YoutubeIcon from "./youtube.svg";
import MezornLogo from "./mezornlogo.svg";
import KissIcon from "./kiss.svg";
import DirectionIcon from "./direction.svg";
import BrainIcon from "./brain.svg";
import LinkedInIcon from "./linkedin.svg";

export {
  NavstickCloseIcon,
  LinkedInIcon,
  DirectionIcon,
  BrainIcon,
  KissIcon,
  MezornLogo,
  ArrowRightIcon,
  AwsIcon,
  CssIcon,
  DialIcon,
  FacebookIcon,
  FlutterIcon,
  HtmlIcon,
  InstagramIcon,
  JavascriptIcon,
  KotlinIcon,
  LinkIcon,
  MapIcon,
  MongodbIcon,
  NavstickIcon,
  NodejsIcon,
  ReactIcon,
  SendIcon,
  SwiftIcon,
  TwitterIcon,
  YoutubeIcon,
};
