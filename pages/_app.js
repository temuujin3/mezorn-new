import "../styles/globals.css";
import { Nav } from "../layout";
import { appWithTranslation } from "next-i18next";
import { useEffect, useRef, useState } from "react";
import { gsap } from "gsap";
import { useRouter } from "next/router";
import { Sidebar } from "../components/Sidebar/Sidebar";
import Image from "next/image";
import Head from "next/head";
import { install } from "ga-gtag";

function MyApp({ Component, pageProps }) {
  const containerRef = useRef(null);
  const pagesRef = useRef();
  const overlayRef = useRef();
  const topRef = useRef();
  const [menuOpen, setMenuOpen] = useState(false);
  const router = useRouter();
  const [canOpen, setCanOpen] = useState(true);
  const [canClose, setCanClose] = useState(false);

  const transitionStart = (e) => {
    // console.log(e)
    if (e.endsWith(router.pathname)) {
      return;
    }
    if (router.pathname == "/career") {
      gsap.to(overlayRef.current, {
        yPercent: -100,
        duration: 0.5,
      });
      gsap.to(topRef.current, {
        yPercent: -100,
        duration: 0.5,
      });
    } else {
      gsap.to(overlayRef.current, {
        yPercent: 0,
        duration: 0.5,
      });
      gsap.to(topRef.current, {
        yPercent: 0,
        duration: 1,
      });
    }
  };
  const transitionEnd = (e) => {
    gsap.to(pagesRef.current, {
      opacity: 1,
      duration: 0.3,
    });
  };

  const toggleMenu = () => {
    if (menuOpen && canClose) {
      setMenuOpen(false);
    }
    if (!menuOpen && canOpen) {
      setMenuOpen(true);
    }
  };

  useEffect(() => {
		install('G-N5X8W6HSPY');
	}, []);

  useEffect(() => {
    gsap.set(overlayRef.current, {
      yPercent: -100,
    });
    gsap.set(topRef.current, {
      yPercent: -100,
    });
  }, []);

  useEffect(() => {
    if(router.pathname == "/career"){
      gsap.to(topRef.current, {
        yPercent: 0,
        duration: 1,
      });
    }

    router.events.on("routeChangeStart", transitionStart);
    router.events.on("routeChangeComplete", transitionEnd);
    router.events.on("routeChangeError", transitionEnd);

    return () => {
      router.events.off("routeChangeStart", transitionStart);
      router.events.off("routeChangeComplete", transitionEnd);
      router.events.off("routeChangeError", transitionEnd);
    };
  }, [router]);

  return (
    <>
      <Head>
        <title>Mezorn</title>
        <meta name="title" content="Mezorn" />
        <meta
          name="description"
          content="Бид таны амьдралын хэмнэлийг технологиор урлана."
        />

        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://mezorn.com/" />
        <meta property="og:title" content="Mezorn" />
        <meta
          property="og:description"
          content="Бид таны амьдралын хэмнэлийг технологиор урлана."
        />
        <meta property="og:image" content="/SEO_COVER.png" />

        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://mezorn.com/" />
        <meta property="twitter:title" content="Mezorn" />
        <meta
          property="twitter:description"
          content="Бид таны амьдралын хэмнэлийг технологиор урлана."
        />
        <meta property="twitter:image" content="/SEO_COVER.png" />
      </Head>
      <div
        ref={containerRef}
        className="relative w-screen min-h-screen overflow-y-visible text-white bg-primary"
      >
        <Sidebar
          isOpen={menuOpen}
          close={() => {
            setMenuOpen(false);
          }}
          setCanClose={setCanClose}
          setCanOpen={setCanOpen}
          canClose={canClose}
        />
        <Nav toggleMenu={toggleMenu} isOpen={menuOpen} />
        <div ref={topRef} className="absolute inset-x-0 z-30 h-80 -top-10">
          <Image
            src={"/images/brandgrids.png"}
            layout="fill"
            objectFit="cover"
            objectposition="0% 100%"
          />
        </div>
        <div
          ref={overlayRef}
          className="fixed z-20 w-screen h-screen bg-primary"
        ></div>
        <div ref={pagesRef}>
          <Component {...pageProps} />
        </div>

      </div>
    </>
  );
}

export default appWithTranslation(MyApp);
