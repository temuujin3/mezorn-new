import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Head from "next/head";
import { MainLayout } from "../layout/MainLayout";
import {
  HtmlIcon,
  JavascriptIcon,
  CssIcon,
  KotlinIcon,
  ReactIcon,
  NodejsIcon,
  MongodbIcon,
  AwsIcon,
  FlutterIcon,
  SwiftIcon,
} from "../assets/icons";

import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import { gsap } from "gsap";
import { useRouter } from "next/router";

const careers = [
  {
    name: "Front-End Developer",
    requiredments: [
      {
        name: "HTML",
        icon: <Image src={HtmlIcon} />,
      },
      {
        name: "CSS",
        icon: <Image src={CssIcon} />,
      },
      {
        name: "Javascript",
        icon: <Image src={JavascriptIcon} />,
      },
      {
        name: "ReactJS",
        icon: <Image src={ReactIcon} />,
      },
    ],
  },
  {
    name: "Back-End Developer",
    requiredments: [
      {
        name: "Javascript",
        icon: <Image src={JavascriptIcon} />,
      },
      {
        name: "Node.js",
        icon: <Image src={NodejsIcon} />,
      },
      {
        name: "MongoDB",
        icon: <Image src={MongodbIcon} />,
      },
      {
        name: "AWS",
        icon: <Image src={AwsIcon} />,
      },
    ],
  },
  {
    name: "Mobile Developer",
    requiredments: [
      {
        name: "Flutter",
        icon: <Image src={FlutterIcon} />,
      },
      {
        name: "Kotlin",
        icon: <Image src={KotlinIcon} />,
      },
      {
        name: "Swift",
        icon: <Image src={SwiftIcon} />,
      },
    ],
  },
];

export async function getStaticProps({ locale }) {
  await waitload(1);
  return {
    props: {
      ...(await serverSideTranslations(locale, ["common"])),
      // Will be passed to the page component as props
    },
  };
}

function waitload(sec) {
  return new Promise((resolve) => setTimeout(resolve, sec * 1000));
}

export default function Career(props) {
  const { t } = useTranslation();
  const infoRef = useRef();
  const router = useRouter();
  const contactRef = useRef();
  const [pageLoaded, setPageLoaded] = useState(false);

  useEffect(() => {
    const infos = gsap.utils.selector(infoRef.current);

    gsap.set(infos(".animate"), {
      y: 100,
      opacity: 0,
    });
    gsap.set(contactRef.current, {
      y: 100,
      opacity: 0,
    });

    setPageLoaded(true);
  }, []);

  useEffect(() => {
    const infos = gsap.utils.selector(infoRef.current);
    
    if (pageLoaded) {
      gsap.to(infos(".animate"), {
        y: 0,
        opacity: 1,
        duration: 0.5,
        stagger: 0.1,
      });
      gsap.to(contactRef.current, {
        y: 0,
        opacity: 1,
        duration: 0.5,
      });
    }
  }, [pageLoaded]);

  useEffect(() => {
    const infos = gsap.utils.selector(infoRef.current);

    const transitionStart = (e) => {
      if (e.endsWith(router.pathname)) {
        return;
      }
      gsap.to(infos(".animate"), {
        opacity: 0,
        duration: 0.5,
      });
      gsap.to(contactRef.current, {
        opacity: 0,
        duration: 0.5,
      });
    };
    const transitionEnd = (e) => {
      gsap.to(infos(".animate"), {
        y: 0,
        opacity: 1,
        duration: 0.5,
        stagger: 0.1,
      });
      gsap.to(contactRef.current, {
        y: 0,
        opacity: 1,
        duration: 0.5,
      });
    };

    router.events.on("routeChangeStart", transitionStart);
    router.events.on("routeChangeComplete", transitionEnd);
    router.events.on("routeChangeError", transitionEnd);

    return () => {
      router.events.off("routeChangeStart", transitionStart);
      router.events.off("routeChangeComplete", transitionEnd);
      router.events.off("routeChangeError", transitionEnd);
    };
  }, [router]);

  return (
    <div>
      <Head>
        <Head>
          <title>We Are Hiring</title>
          <meta name="title" content="Mezorn" />
          <meta
            name="description"
            content="БИДЭНТЭЙ НЭГДЭЖ ХАМТДАА ХӨГЖИЦГӨӨЕ."
          />

          <meta property="og:type" content="website" />
          <meta property="og:url" content="https://mezorn.com/" />
          <meta property="og:title" content="Mezorn" />
          <meta
            property="og:description"
            content="БИДЭНТЭЙ НЭГДЭЖ ХАМТДАА ХӨГЖИЦГӨӨЕ."
          />
          <meta property="og:image" content="/hiring.png" />

          <meta property="twitter:card" content="summary_large_image" />
          <meta property="twitter:url" content="https://mezorn.com/" />
          <meta property="twitter:title" content="Mezorn" />
          <meta
            property="twitter:description"
            content="БИДЭНТЭЙ НЭГДЭЖ ХАМТДАА ХӨГЖИЦГӨӨЕ."
          />
          <meta property="twitter:image" content="/hiring.png" />
        </Head>
      </Head>

      <MainLayout>
        <div className="relative z-40 flex flex-col col-span-10 col-start-2 pt-32 overflow-hidden lg:pt-40 lg:grid lg:grid-cols-2">
          <div ref={infoRef} className="flex flex-col space-y-5">
            <div className="text-lg animate text-brand">
              {t("common:career.name")}
            </div>
            <div className="text-3xl font-black uppercase lg:text-6xl animate">
              {t("common:career.title")}
            </div>
            <div className="text-xl lg:text-2xl animate">
              {t("common:career.description")}
            </div>
            <div className="flex flex-col space-y-2">
              {careers.map((i) => {
                return (
                  <div
                    key={i.name}
                    className="relative flex flex-col p-5 mt-5 overflow-hidden rounded-lg lg:mt-10 animate"
                  >
                    <div className="absolute inset-0 -z-10 bg-gradient-to-r from-brand to-transparent opacity-10"></div>
                    <div className="text-sm font-bold uppercase lg:text-lg">
                      {i.name}
                    </div>
                    <div className="flex flex-row flex-wrap mt-3">
                      {i.requiredments.map((item) => {
                        return (
                          <div
                            key={item.name}
                            className="flex flex-row mt-2 mr-5 space-x-2 text-xs lg:text-base"
                          >
                            {item.icon}
                            <div>{item.name}</div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div
            ref={contactRef}
            className="flex flex-col items-end justify-end py-10 lg:py-0"
          >
            <div className="text-2xl font-bold text-center lg:text-right">
              <a
                href={`mailto:${t("common:career.mail")}`}
                className="text-brand"
              >
                {t("common:career.mail")}
              </a>
              &nbsp;
              {t("common:career.guide")}
            </div>
          </div>
        </div>
      </MainLayout>
    </div>
  );
}
