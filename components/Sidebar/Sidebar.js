import Image from "next/image";
import {
  FacebookIcon,
  InstagramIcon,
  LinkedInIcon,
  TwitterIcon,
  YoutubeIcon,
} from "../../assets/icons";

import { useRef, useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";
import { gsap } from "gsap";
import Link from "next/link";

const menus = [
  {
    name: "common:nav.1",
    target: "/#wwd",
  },
  {
    name: "common:nav.2",
    target: "/#pp",
  },
  {
    name: "common:nav.3",
    target: "/career",
  },
  {
    name: "common:nav.4",
    target: "/#blog",
  },
  {
    name: "common:nav.5",
    target: "/#contact",
  },
];

const social = [
  {
    icon: TwitterIcon,
    url: "https://twitter.com/MezornLLC",
  },
  {
    icon: FacebookIcon,
    url: "https://www.facebook.com/mezorn",
  },
  {
    icon: YoutubeIcon,
    url: "https://www.youtube.com/channel/UC2DSVuyln8sDhh4NqHe6BWg",
  },
  {
    icon: InstagramIcon,
    url: "https://www.instagram.com/mezornteam/",
  },
  {
    icon: LinkedInIcon,
    url: "https://www.linkedin.com/company/mezorn/mycompany/",
  },
];

export const Sidebar = ({ isOpen, close, setCanClose, setCanOpen, canClose }) => {
  const { t } = useTranslation();
  const container = useRef();
  const router = useRouter();

  useEffect(() => {
    const items = gsap.utils.selector(container.current);

    if (container.current) {
      gsap.set(items("#background"), {
        opacity: 0,
      });
      gsap.set(items("#panel"), {
        x: "100%",
      });
      gsap.set(items("#sidebar .item"), {
        x: 20,
        opacity: 0,
      });
    }
  }, []);

  useEffect(() => {
    const items = gsap.utils.selector(container.current);

    if (container.current) {
      if (isOpen) {
        gsap.to(items("#background"), {
          opacity: 1,
          duration: 1,
        });
        gsap.to(items("#panel"), {
          x: 0,
          duration: 0.3,
        });
        gsap.to(items("#sidebar .item"), {
          x: 0,
          opacity: 1,
          delay: 0.3,
          duration: 0.1,
          stagger: 0.055,
          onComplete: () => {
            setCanOpen(false);
            setCanClose(true);
          },
        });
      }
      if (!isOpen) {
        gsap.to(items("#background"), {
          opacity: 0,
          duration: 1,
        });
        gsap.to(items("#panel"), {
          x: "100%",
          delay: 0.2,
          duration: 0.6,
          onComplete: () => {
            setCanClose(false);
            setCanOpen(true);
          },
        });
        gsap.to(items("#sidebar .item"), {
          x: 20,
          opacity: 0,
          duration: 0,
        });
      }
    }

    return () => {};
  }, [isOpen]);

  useEffect(() => {
    router.events.on("routeChangeStart", close);

    return () => {
      router.events.off("routeChangeStart", close);
    };
  }, [router]);

  return (
    <div
      ref={container}
      className={`fixed inset-0 z-50 flex flex-row justify-end ${
        !isOpen && "pointer-events-none"
      }`}
    >
      <div
        onClick={close}
        id="background"
        className="absolute inset-0 z-0 bg-black bg-opacity-50"
      ></div>
      <div
        id="panel"
        className="absolute right-0 z-10 w-3/4 h-screen bg-black md:w-1/2 lg:w-1/3"
      ></div>
      <div
        id="sidebar"
        className="z-20 flex flex-col w-3/4 h-screen p-10 overflow-hidden text-xl md:w-1/2 lg:w-1/3"
      >
        <div className="h-40 text-white pt-14 item"></div>
        {menus.map((i, index) => {
          return (
            <Link key={i.name} href={canClose ? i.target : ''}>
              <a onClick={canClose && close} className="mt-4 text-xl transition lg:text-3xl item sm:mt-7 hover:text-brand durtaion-300">
                {t(i.name)}
              </a>
            </Link>
          );
        })}

        <div className="flex flex-row mt-10 space-x-5 lg:mt-20">
          {social.map((i, index) => {
            return (
              <a key={i.url} href={i.url} target="_blank" className="item">
                <Image src={i.icon} />
              </a>
            );
          })}
        </div>
        <div className="mt-10 text-sm lg:mt-20 item lg:text-base 2xl:text-lg">{t("common:footer.address.address")}</div>
        <div className="flex flex-col mt-10 text-sm font-bold lg:space-x-10 lg:flex-row item 2xl:text-lg">
          <a
            href={"mailto:" + t("common:footer.email")}
            className="py-3 border-b border-b-brand"
          >
            {t("common:footer.email")}
          </a>

          <a href={"tel:" + t("common:footer.phone")} className="py-3">
            {t("common:footer.phone")}
          </a>
        </div>
      </div>
    </div>
  );
};
