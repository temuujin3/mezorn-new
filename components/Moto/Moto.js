import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import { BrainIcon, KissIcon, DirectionIcon } from "../../assets/icons";
import { useTranslate } from "../../hooks/useTranslate";

export const Moto = () => {
  const { t } = useTranslation();

  const distance = useTranslate();

  const motos = [
    {
      icon: BrainIcon,
      name: t(`common:moto-1`),
      index: 5,
    },
    {
      icon: DirectionIcon,
      name: t(`common:moto-2`),
      index: 20,
    },
    {
      icon: KissIcon,
      name: t(`common:moto-3`),
      index: 8,
    },
  ];

  return (
    <div className="flex flex-col col-span-12 mt-20 mb-20 overflow-hidden">
      {motos.map((i, index) => {
        return (
          <div
            key={i.icon + index}
            className="relative flex items-center w-full h-14 sm:h-20 border-y border-morepassive"
          >
            <div className="absolute inset-y-0 z-10 flex items-center w-40 pl-5 left:0 bg-gradient-to-r from-primary to-transparent">
              <Image src={i.icon} />
            </div>
            {
            }
            <div
              className={`z-0 flex flex-row space-x-3 overflow-visible text-2xl sm:text-4xl font-black uppercase transition duration-300 ease-out flex-nowrap -ml-[1500px]`}
              style={{
                transform: index == 1 && distance ? distance.replace('X(', 'X(-') : distance,
              }}
            >
              {[...Array(40).keys()].map((item, index) => {
                return (
                  <div
                    key={item + index}
                    className={
                      i.index == item
                        ? "text-white whitespace-nowrap"
                        : "opacity-10 text-primary text-outline whitespace-nowrap"
                    }
                  >
                    {i.name}
                  </div>
                );
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};
