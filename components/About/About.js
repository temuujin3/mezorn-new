import { useTranslation } from "next-i18next";
import { useState, useEffect, useRef } from "react";
import Image from "next/image";
import { gsap } from "gsap";

export const About = () => {
  const { t } = useTranslation();
  const [currentSlide, setCurrentSlide] = useState(-1);
  const slideContainer = useRef();
  const infoContainer = useRef();

  const slides = [
    {
      key: "slide1",
      coverUrl: "/images/edutech.jpg",
      productUrls: ["/images/whitelogos/tetgeleg.png"],
    },
    {
      key: "slide2",
      coverUrl: "/images/blockchain.jpg",
      productUrls: [
        "/images/whitelogos/mapmn.png",
        "/images/whitelogos/karvuon.png",
      ],
    },
    {
      key: "slide3",
      coverUrl: "/images/lifestyle.jpg",
      productUrls: [
        "/images/whitelogos/ubcab.png",
        "/images/whitelogos/ubeats.png",
        "/images/whitelogos/ubcall.png",
      ],
    },
    {
      key: "slide4",
      coverUrl: "/images/media.jpg",
      productUrls: [
        "/images/whitelogos/mentorhub.png",
        "/images/whitelogos/itoim.png",
      ],
    },
  ];

  const options = {
    root: null,
    rootMargin: "-50%",
  };

  const observerCallback = (entries) => {
    for (var i of entries) {
      if (i.isIntersecting) {
        setCurrentSlide(i.target.id);
      }
    }
  };

  useEffect(() => {
    const observer = new IntersectionObserver(observerCallback, options);
    if (slideContainer.current) {
      for (let i = 0; i < slideContainer.current.children.length; i++)
        observer.observe(slideContainer.current.children[i].firstChild);
    }
    return () => {
      if (slideContainer.current) {
        for (let i = 0; i < slideContainer.current.children.length; i++)
          observer.unobserve(slideContainer.current.children[i].firstChild);
      }
    };
  }, []);

  useEffect(() => {
    const el = gsap.utils.selector(infoContainer.current);

    gsap.set(el(".stagger"), {
      opacity: 0,
      y: 10,
    });
    gsap.to(el(".stagger"), {
      opacity: 1,
      y: 0,
      duration: 1,
      stagger: 0.15,
    });

    return () => {
      gsap.killTweensOf(el);
    };
  }, [currentSlide]);

  return (
    <div
      className="relative grid col-span-12 md:col-span-10 md:col-start-2 md:grid-cols-2"
      id="slides"
    >
      <div className="absolute z-10 w-full h-full p-5 md:p-0 md:static">
        {slides[currentSlide] && (
          <div
            className="sticky top-0 flex flex-col items-start justify-center h-screen space-y-20 sm:pr-40 md:pr-10 ultra:pr-80"
            ref={infoContainer}
            id="wwd"
          >
            <div className="flex flex-col space-y-5">
              <div className="text-brand"> {t(`common:about.name`)}</div>
              <div className="text-3xl font-black uppercase stagger">
                {t(`common:about.data.${slides[currentSlide].key}.title`)}
              </div>
              <div className="h-40 text-white stagger md:text-customgray">
                {t(`common:about.data.${slides[currentSlide].key}.description`)}
              </div>
            </div>
            <div className="flex flex-wrap">
              {slides[currentSlide].productUrls.map((i) => {
                return (
                  <img
                    key={i}
                    src={i}
                    className="object-scale-down w-auto h-5 mb-5 mr-5 md:mr-10 stagger"
                  />
                );
              })}
            </div>
            <div className="flex flex-row items-center space-x-3">
              {slides.map((i, index) => {
                return (
                  <div
                    key={index}
                    className={`transition duration-300 w-10 md:w-16 border-b ${
                      index == currentSlide
                        ? "border-b-white md:border-b-brand"
                        : "border-b-white/20 md:border-b-passive"
                    }`}
                  ></div>
                );
              })}
            </div>
          </div>
        )}
      </div>
      <div ref={slideContainer} className="z-0">
        {slides.map((i, index) => {
          return (
            <div
              key={i.key}
              className="flex items-center justify-center w-full h-screen"
            >
              <div
                id={index}
                className="relative w-full h-screen md:h-2/3 brightness-50 md:opacity-100 opacity-60"
              >
                <Image src={i.coverUrl} layout="fill" objectFit="cover" />
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
