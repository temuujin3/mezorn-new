import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useRef, useEffect, useState } from "react";
import { gsap } from "gsap";

const importAll = (r) => {
  let images = [];

  r.keys().map((item, index) => {
    images[index] = item.replace("./", "/images/whitelogos/");
  });
  return images;
};

export const Products = ({size }) => {
  const { t } = useTranslation();

  const images = importAll(
    require.context(
      "../../public/images/whitelogos",
      false,
      /\.(png|jpe?g|svg)$/
    )
  );
  const wrapper = useRef();
  const boxes = useRef();
  const box = gsap.utils.selector(boxes);
  // const animateCarousel = (boxWidth, wrapWidth) => {
  //   gsap.to(boxes, {
  //     duration: boxes.length * 0.5,
  //     ease: "none",
  //     x: "+=" + boxWidth,
  //     modifiers: {
  //       x: gsap.utils.unitize(gsap.utils.wrap(-boxWidth, wrapWidth)),
  //     },
  //     repeat: -1,
  //   });
  // };

  useEffect(() => {
    const boxWidth = boxes.current.firstChild.offsetWidth + 50;
    const boxHeigth = boxes.current.firstChild.offsetHeight;
    const totalWidth = boxWidth * (images.length - 1);

    gsap.set(box(".infinite-loop-slide"), {
      x: (i) => i * boxWidth,
      y: (i) => i * -boxHeigth,
    });

    gsap.to(box(".infinite-loop-slide"), {
      duration: boxWidth * 0.5,
      x: "-=" + (totalWidth + boxWidth),
      paused: false,
      ease: "none",
      repeat: -1,
      modifiers: {
        x: gsap.utils.unitize(gsap.utils.wrap(-boxWidth, totalWidth)),
      },
    });
  }, [size]);

  return (
    <div
      ref={wrapper}
      className="flex flex-col col-span-12 py-6 space-y-10 sm:py-10 border-y border-passive"
      id="pp"
    >
      <div className="mx-auto mt-5 text-lg text-center sm:pt-0">{t("common:products")}</div>
      <div
        ref={boxes}
        className="opacity-50 max-h-16 infinite-loop-container flex-nowrap overflow-hidden"
      >
        {images.map((i, index) => {
          return (
            <div
              key={i + index}
              className="relative flex-shrink-0 h-10 sm:h-16 infinite-loop-slide w-14 sm:w-28"
            >
              <Image src={i} layout="fill" objectFit="scale-down" />
            </div>
          );
        })}
      </div>
    </div>
  );
};
