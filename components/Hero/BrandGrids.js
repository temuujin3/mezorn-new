import { gsap } from "gsap";
import { useEffect, useRef, useState } from "react";
import useInterval from "../../hooks/useInterval";

const products = [
  {
    1: [
      {
        url: "/images/whitelogos/pickpack.png",
        color: "#0093B2",
      },
      {
        url: "/images/whitelogos/itoim.png",
        color: "#E24616",
      },
    ],
    7: [
      {
        url: "/images/whitelogos/zary.png",
        color: "#FF365D",
      },
      {
        url: "/images/whitelogos/bankassoc.png",
        color: "#0166B3",
      },
    ],
    13: [
      {
        url: "/images/whitelogos/worldbank.png",
        color: "#002F53",
      },
      {
        url: "/images/whitelogos/ubcall.png",
        color: "#19B17B",
      },
    ],
    23: [
      {
        url: "/images/whitelogos/unicef.png",
        color: "#1EABE2",
      },
      {
        url: "/images/whitelogos/mongolshuudan.png",
        color: "#E21B21",
      },
    ],
    28: [
      {
        url: "/images/whitelogos/ubeats.png",
        color: "#2A9E60",
      },
      {
        url: "/images/whitelogos/hurunguoruulagch.png",
        color: "#1C4392",
      },
    ],
  },
  {
    5: [
      {
        url: "/images/whitelogos/monos.png",
        color: "#462DB3",
      },
      {
        url: "/images/whitelogos/mapmn.png",
        color: "#FD373D",
      },
    ],
    10: [
      {
        url: "/images/whitelogos/ubcab.png",
        color: "#FFD300",
      },
      {
        url: "/images/whitelogos/bsb.png",
        color: "#00358E",
      },
    ],
    24: [
      {
        url: "/images/whitelogos/tengerdaatgal.png",
        color: "#F46144",
      },
      {
        url: "/images/whitelogos/lifehack.png",
        color: "#FF330A",
      },
    ],
    26: [
      {
        url: "/images/whitelogos/tetgeleg.png",
        color: "#0371BC",
      },
      {
        url: "/images/whitelogos/toc.png",
        color: "#1D4792",
      },
    ],
    30: [
      {
        url: "/images/whitelogos/mentorhub.png",
        color: "#2F2F2F",
      },
      {
        url: "/images/whitelogos/shiidel.png",
        color: "#062F5F",
      },
    ],
  },
];

function getRandomItem(arr) {
  // get random index value
  const randomIndex = Math.floor(Math.random() * arr.length);

  // get random item
  const item = arr[randomIndex];

  return item;
}

export const BrandGrids = () => {
  const container = useRef();
  const [rand, setRand] = useState(0);
  const [current, setCurrent] = useState(0);

  useInterval(() => {
    setCurrent(getRandomItem([1, 7, 13, 23, 28, 5, 10, 24, 26, 30]));
  }, 3500);

  return (
    <div
      ref={container}
      className={`grid w-full ultra:grid-cols-8 grid-cols-2`}
    >
      <div className="hidden grid-cols-3 ultra:grid">
        {[...Array(12)].map((i, index) => {
          return (
            <div
              key={"firstChunk" + index}
              className="w-full border border-morepassive aspect-square"
            ></div>
          );
        })}
      </div>
      {products.map((chunk, index) => {
        return (
          <div
            key={"chunk" + index}
            className={`grid grid-cols-9 ultra:col-span-3 col-span-2 sm:col-span-1`}
          >
            {[...Array(34).keys()].map((i) => {
              if (index == 0) {
                return (
                  <div
                    key={i}
                    className={`w-full border border-morepassive ${
                      i == 23 || i == 28 ? "col-span-2" : "aspect-square"
                    }`}
                  >
                    {[1, 7, 13, 23, 28].includes(i) && (
                      <div className="relative w-full h-full overflow-hidden">
                        {products[index][i].map((face, index) => {
                          return (
                            <div
                              key={face.url + index}
                              className={`transition duration-1000 absolute w-full h-full px-1 sm:px-2 lg:px-4 2xl:px-7 transform ${
                                index == 0 ? "top-0" : "top-full"
                              } ${
                                i == current
                                  ? "-translate-y-full"
                                  : "translate-y-0"
                              }`}
                              style={{
                                backgroundColor: face.color,
                              }}
                            >
                              <div className="relative flex items-center justify-center w-full h-full">
                                <img
                                  src={face.url}
                                  className="object-scale-down"
                                />
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    )}
                  </div>
                );
              }
              return (
                <div
                  key={i}
                  className={`w-full border border-morepassive ${
                    i == 5 || i == 30 ? "col-span-2" : "aspect-square"
                  }`}
                >
                  {[5, 10, 24, 26, 30].includes(i) && (
                    <div className="relative w-full h-full overflow-hidden">
                      {products[index][i].map((face, index) => {
                        return (
                          <div
                            key={face.url + index}
                            className={`transition duration-1000 absolute w-full h-full px-1 sm:px-2 lg:px-4 2xl:px-7 transform ${
                              index == 0 ? "top-0" : "top-full"
                            } ${
                              i == current
                                ? "-translate-y-full"
                                : "translate-y-0"
                            }`}
                            style={{
                              backgroundColor: face.color,
                            }}
                          >
                            <div className="relative flex items-center justify-center w-full h-full">
                              <img
                                src={face.url}
                                className="object-scale-down"
                              />
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  )}
                </div>
              );
            })}
          </div>
        );
      })}
      <div className="hidden grid-cols-3 ultra:grid">
        {[...Array(12).keys()].map((i, index) => {
          return (
            <div
              key={"secondChunk" + index}
              className="w-full border border-morepassive aspect-square"
            ></div>
          );
        })}
      </div>
    </div>
  );
};
