import { DialIcon, MezornLogo } from "../../assets/icons";
import { useTranslation } from "next-i18next";
import Image from "next/image";
import { CTAButton } from "../CTAButton";
import { BrandGrids } from "./BrandGrids";

export const Hero = () => {
  const { t } = useTranslation();
  return (
    <div className="flex flex-col justify-between min-h-screen col-span-12 space-y-10 text-center">
      <div className="flex flex-col items-center w-full p-10 mx-auto mt-32 space-y-10 ultra:mt-44 sm:p-0 sm:w-1/2 md:w-3/4 lg:w-2/3 2xl:w-1/3 ultra:w-1/2">
        <div className="w-1/2 sm:w-full">
          <Image src={MezornLogo} />
        </div>
        <h1
          className={`font-black uppercase ultra:text-7xl text-2xl sm:text-3xl lg:text-4xl xl:text-5xl`}
        >
          {t("common:title")}
        </h1>
        <CTAButton icon={<Image src={DialIcon} />} text={t("common:cta")} />
      </div>
      <BrandGrids />
    </div>
  );
};
