import Image from "next/image";
import { useState, useEffect, useRef } from "react";
import { gsap } from "gsap";
import { ArrowRightIcon } from "../../assets/icons";
import { useTranslation } from "next-i18next";

const teams = [
  {
    url: "/images/teams/team-1.jpg",
    name: "UX/UI Team",
  },
  {
    url: "/images/teams/team-2.jpg",
    name: "Front-End Team",
  },
  {
    url: "/images/teams/team-3.jpg",
    name: "Back-End Team",
  },
  {
    url: "/images/teams/team-4.jpg",
    name: "Mobile Team",
  },
  {
    url: "/images/teams/team-5.jpg",
    name: "Business Team",
  },
];

export const Team = () => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const slide = useRef();


  const { t } = useTranslation();

  const changeSlide = (index) => {
    if (index < 0 || index >= teams.length) {
      return;
    }

    setCurrentSlide(index);
  };

  useEffect(() => {
    gsap.fromTo(
      slide.current,
      {
        opacity: 0,
      },
      {
        opacity: 1,
        duration: 1.5,
      }
    );

    return () => {};
  }, [currentSlide]);
  return (
    <div className="col-span-12 mt-10 lg:col-start-2 lg:col-span-10">
      {/* <div className="flex flex-row mt-20 space-x-2 overflow-scroll">
        {teams.map((i, index) => {
          return (
            <div
              key={i.name}
              onClick={() => {
                setCurrentSlide(index);
              }}
              className={`cursor-pointer p-5 text-center min-w-max border-b transition duration-500 ${
                index == currentSlide
                  ? "border-b-brand"
                  : "border-b-morepassive"
              }`}
            >
              {i.name}
            </div>
          );
        })}
      </div> */}
      <div className="flex flex-row items-center justify-between px-5 lg:px-0">
        <div className="text-xl font-bold uppercase lg:text-3xl">{t("common:team")}</div>
        <div className="flex flex-row h-10 lg:h-16">
          <div
            onClick={() => {
              changeSlide(currentSlide - 1);
            }}
            className={`cursor-pointer transition duration-300 flex items-center justify-center h-full rotate-180 border aspect-square border-passive ${currentSlide == 0 && 'opacity-30'}`}
          >
            <Image src={ArrowRightIcon} />
          </div>
          <div
            onClick={() => {
              changeSlide(currentSlide + 1);
            }}
            className={`cursor-pointer transition duration-300 flex items-center justify-center h-full border aspect-square border-passive ${currentSlide == teams.length - 1 && 'opacity-30'}`}
          >
            <Image src={ArrowRightIcon} />
          </div>
        </div>
      </div>
      <div className="relative">
      <img
        ref={slide}
        className="w-full h-96 lg:h-[65vh] xl:h-[75vh] relative object-cover"
        src={teams[currentSlide].url}
        quality={50}
        objectposition="0% 30%"
      />
      <div className="absolute inset-0 flex flex-row items-end justify-start p-5 bg-gradient-to-t from-black/80 to-transparent">
            <div className="text-2xl font-black uppercase">{teams[currentSlide].name}</div>
      </div>
      </div>
    </div>
  );
};
