import Image from "next/image";
import Link from "next/link";
import { ArrowRightIcon } from "../assets/icons";

export const HireCard = ({ text }) => {
  return (
    <Link href="/career">
      <div className="flex flex-row items-center p-3 px-4 space-x-3 text-xs uppercase transition duration-300 rounded-full cursor-pointer sm:p-4 sm:px-6 group hover:bg-opacity-50 bg-brand bg-opacity-20">
        <p className="transition duration-300 transform group-hover:rotate-12">
          👋
        </p>
        <p> {text}</p>
        <div>
          <Image src={ArrowRightIcon} />
        </div>
      </div>
    </Link>
  );
};
