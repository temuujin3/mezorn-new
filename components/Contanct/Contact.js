import { useTranslation } from "next-i18next";
import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import emailjs from "@emailjs/browser";

import {
  FacebookIcon,
  InstagramIcon,
  LinkedInIcon,
  TwitterIcon,
  YoutubeIcon,
  MapIcon,
  SendIcon,
} from "../../assets/icons";
import { AnimatedBottomBorder } from "../AnimatedBottomBorder";

import { CTAButton } from "../CTAButton";

const social = [
  {
    icon: TwitterIcon,
    url: "https://twitter.com/MezornLLC",
  },
  {
    icon: FacebookIcon,
    url: "https://www.facebook.com/mezorn",
  },
  {
    icon: YoutubeIcon,
    url: "https://www.youtube.com/channel/UC2DSVuyln8sDhh4NqHe6BWg",
  },
  {
    icon: InstagramIcon,
    url: "https://www.instagram.com/mezornteam/",
  },
  {
    icon: LinkedInIcon,
    url: "https://www.linkedin.com/company/mezorn/mycompany/",
  },
];

export const Contact = () => {
  const { t } = useTranslation();
  const form = useRef();
  const [name, setName] = useState();
  const [phone, setPhone] = useState();
  const [email, setEmail] = useState();
  const [message, setMessage] = useState();
  const [toastSuccess, setToastSuccess] = useState(false);
  const [showToast, setShowToast] = useState(false);

  const success = () => {
    setToastSuccess(true);
    setShowToast(true);

    setTimeout(() => {
      setShowToast(false);
    }, 3000);
  };

  const fail = () => {
    setToastSuccess(false);
    setShowToast(true);

    setTimeout(() => {
      setShowToast(false);
    }, 3000);
  };

  useEffect(() => {
    return () => {};
  }, []);

  const forms = [
    {
      name: "common:footer.forms.1.name",
      placeholder: "common:footer.forms.1.name",
      type: "text",
      callback: setName,
    },
    {
      name: "common:footer.forms.2.name",
      placeholder: "common:footer.forms.2.placeholder",
      type: "number",
      callback: setPhone,
    },
    {
      name: "common:footer.forms.3.name",
      placeholder: "common:footer.forms.3.placeholder",
      type: "email",
      callback: setEmail,
    },
    {
      name: "common:footer.forms.4.name",
      placeholder: "common:footer.forms.4.placeholder",
      type: "text",
      callback: setMessage,
    },
  ];

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      name.trim() === "" ||
      phone.trim() === "" ||
      email.trim() === "" ||
      message.trim() === ""
    ) {
      alert("Та талбараа бүрэн бөглөнө үү...");
    } else {
      emailjs
        .send(
          "service_bx0920q",
          "template_sjtwflh",
          {
            name: name,
            phone: phone,
            email: email,
            message: message,
          },
          "user_IyY8N04IQuJpcmGxtAVSr"
        )
        .then(
          function (response) {
            // console.log("SUCCESS!", response.status, response.text);
            success();
          },
          function (error) {
            fail();
            // console.log("FAILED...", error);
          }
        );
    }
  };

  return (
    <>
      <div
        className={`fixed inset-x-0 z-50 flex items-center justify-center transition duration-300 bottom-10 transform pointer-events-none ${
          showToast ? "translate-y-0" : "translate-y-96"
        }`}
      >
        <div
          className={`p-5 border rounded-full ${
            toastSuccess && "bg-green-400/20 border-green-400"
          }`}
        >
          {toastSuccess ? t("common:toast.success") : t("common:toast.fail")}
        </div>
      </div>
      <div
        className="relative grid min-h-screen grid-cols-12 col-span-12 overflow-hidden bg-black"
        id="contact"
      >
        <video
          autoPlay
          playsInline
          muted
          loop
          preload="true"
          className="absolute top-0 left-0 object-cover w-full h-full brightness-50"
        >
          <source src={"https://cdn.mzn.mn/intro/intro.mp4"} />
        </video>
        <div className="absolute inset-0 z-10 bg-black bg-opacity-80"></div>
        <div className="z-20 grid content-start grid-cols-5 col-span-10 col-start-2 py-20 overflow-hidden lg:py-40 gap-x-40 gap-y-10 sm:gap-y-20 lg:gap-y-10">
          <div className="col-span-5 text-3xl font-black uppercase lg:text-5xl h-max">
            {t("common:footer.title")}
          </div>
          <form
            ref={form}
            className="flex flex-col col-span-5 space-y-5 lg:col-span-3 "
          >
            {forms.map((i) => {
              return (
                <div key={i.name} className="relative flex flex-col">
                  <label className="text-sm font-black uppercase">
                    {t(i.name)}
                  </label>
                  <input
                    id={i.name}
                    type={i.type}
                    onChange={(e) => i.callback(e.target.value)}
                    placeholder={t(i.placeholder)}
                    className="py-2 text-lg bg-transparent border-b peer border-b-passive focus:ring-0 focus:outline-0"
                    required
                  ></input>
                  <AnimatedBottomBorder trigger={"focus"} />
                </div>
              );
            })}

            <CTAButton
              type={"submit"}
              onClick={handleSubmit}
              text={t("common:footer.button")}
              icon={<Image src={SendIcon} />}
            />
          </form>
          <div className="flex flex-col col-span-5 space-y-5 lg:col-span-2">
            <div className="text-sm font-black uppercase">
              {t("common:footer.social")}
            </div>
            <div className="flex flex-row space-x-5">
              {social.map((i, index) => {
                return (
                  <a key={i.url} href={i.url} target="_blank">
                    <Image src={i.icon} />
                  </a>
                );
              })}
            </div>
            <div className="text-sm font-black uppercase">
              {t("common:footer.address.name")}
            </div>
            <div>{t("common:footer.address.address")}</div>

            <a
              href="https://goo.gl/maps/a2P7YKL2puuoZV6b7"
              target="_blank"
              rel="noopener noreferrer"
              className="flex flex-row items-center p-5 space-x-5 text-sm rounded-md cursor-pointer pr-7 w-max bg-opacity-20 bg-brand"
            >
              <Image src={MapIcon} />{" "}
              <div className="uppercase">{t("common:footer.map")}</div>
            </a>
            <div className="flex flex-row space-x-10 font-bold">
              <a
                href={"mailto:" + t("common:footer.email")}
                className="py-3 border-b border-b-brand"
              >
                {t("common:footer.email")}
              </a>

              <a href={"tel:" + t("common:footer.phone")} className="py-3">
                {t("common:footer.phone")}
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
