export const AnimatedBottomBorder = ({trigger = 'hover'}) => {
    return (
        <span className={`absolute w-0 ${trigger == 'hover' ? 'group-hover:w-full' : 'peer-focus:w-full'} border-b bottom-0 border-brand transition-all duration-700`}></span>
    );
}