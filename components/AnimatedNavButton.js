export const AnimatedNavButton = ({isOpen}) => {
  return (
    <div className="relative h-7 sm:w-10 sm:h-10 w-7">
      <div className={`absolute w-7 sm:w-10 h-0.5 bg-white transition-all duration-300 transform ${isOpen ? 'rotate-45 top-4 sm:top-5': 'top-1 sm:top-2'}`}></div>
      <div className={`absolute w-7 sm:w-10 h-0.5 bg-white transition-all duration-300 transform ${isOpen ? '-rotate-45 top-4 sm:top-5': 'top-6 sm:top-8'}`}></div>
    </div>
  );
};
