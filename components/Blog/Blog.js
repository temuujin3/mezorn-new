import { useTranslation } from "next-i18next";
import Image from "next/image";
import { LinkIcon } from "../../assets/icons";
import { AnimatedBottomBorder } from "../AnimatedBottomBorder";

const blogs = [
  {
    name: "common:blog.data.1",
    url: "https://mezorn.medium.com/%D1%82%D0%B5%D1%85%D0%BD%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D0%B9%D0%BD-%D0%BC%D0%B0%D1%81%D1%82%D0%B5%D1%80%D1%83%D1%83%D0%B4%D0%B0%D0%B0%D1%81-%D1%81%D1%83%D1%80%D0%B0%D0%BB%D1%86%D0%B0%D1%85-%D1%83%D1%83-%D1%85%D2%AF%D1%80%D1%8D%D1%8D%D0%B4-%D0%B8%D1%80%D1%8D%D1%8D-f2f97aa5b55a",
  },
  {
    name: "common:blog.data.2",
    url: "https://mezorn.medium.com/mezorn-%D1%82%D0%BE%D0%B9-%D1%85%D0%B0%D0%BC%D1%82-%D0%B4%D0%B0%D1%80%D0%B0%D0%B0%D0%B3%D0%B8%D0%B9%D0%BD-%D0%BA%D0%B0%D1%80%D1%8C%D0%B5%D1%80%D0%B0%D0%B0-%D0%B1%D2%AF%D1%82%D1%8D%D1%8D%D0%B5-43cdc1ce13af",
  },
  {
    name: "common:blog.data.3",
    url: "https://mezorn.medium.com/%D0%B1%D0%B8%D0%B4%D1%8D%D0%BD%D1%82%D1%8D%D0%B9-%D0%BD%D1%8D%D0%B3%D0%B4-85e3fc94d962",
  },
];

export const Blog = () => {
  const { t } = useTranslation();
  return (
    <div
      className="flex flex-col col-span-10 col-start-2 py-20 space-y-10 sm:py-40"
      id="blog"
    >
      <div className="text-3xl font-black uppercase">
        {t("common:blog.name")}
      </div>
      <div className="flex flex-col space-y-3">
        {blogs.map((i, index) => {
          return (
            <a
              href={i.url}
              target="_blank"
              key={i.url}
              className="relative flex flex-row items-center justify-between w-full transition duration-300 border group border-passive hover:border-brand"
            >
              <div className="p-2 text-base font-semibold uppercase sm:text-xl sm:p-5 grow">
                {t(i.name)}
              </div>
              <div className="flex items-center justify-center h-full p-2 transition duration-300 border-l sm:p-5 border-l-passive group-hover:border-l-brand">
                <div className="relative w-6 h-6 shrink-0">
                  <Image src={LinkIcon} />
                </div>
              </div>
            </a>
          );
        })}
      </div>
    </div>
  );
};
