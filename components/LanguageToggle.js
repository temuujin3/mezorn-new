import Link from "next/link";
import { useRouter } from "next/router";

const LanguageToggle = () => {
  const router = useRouter();

  return (
    <Link
      href={'/'} locale={ router.locale == "en" ? 'mn' : 'en'}
    >
      <div className="relative flex flex-row text-xs font-black rounded-md cursor-pointer bg-primary w-max">
        <a className="z-10 flex items-center justify-center w-8 h-8 rounded-md">
          EN
        </a>
        <a className="z-10 flex items-center justify-center w-8 h-8 rounded-md">
          MN
        </a>
        <div
          className={`absolute inset-y-0 z-0 w-1/2 transition-all duration-300 rounded-md bg-brand ${
            router.locale == "en" ? "left-0" : "right-0"
          }`}
        ></div>
      </div>
    </Link>
  );
};

export default LanguageToggle;
