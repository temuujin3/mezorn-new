import Image from "next/image";
import Link from "next/link";
import { ArrowRightIcon } from "../assets/icons";

export const CTAButton = ({ icon, text, onClick, ...rest }) => {
  return (
    <Link href={"/#contact"}>
      <button
        {...rest}
        onClick={onClick}
        className="flex flex-row items-center p-4 px-5 space-x-5 text-xs tracking-widest uppercase transition duration-300 border rounded-full sm:p-5 sm:text-sm hover:bg-opacity-50 sm:px-7 bg-brand bg-opacity-20 border-brand w-max"
      >
        {icon}
        <p>{text}</p>
        <div>
          <Image src={ArrowRightIcon} />
        </div>
      </button>
    </Link>
  );
};
