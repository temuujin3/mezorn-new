const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./layout/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        brand: "#F41435",
        primary: "#1E1E1E",
        passive: "#323232",
        morepassive: "#222222",
        customgray: "#AAAAAA",
      },
    },
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
      "2xl": "1536px",
      ultra: "1980px",
    },
    fontFamily: {
      sans: ["GIP", ...defaultTheme.fontFamily.sans],
    },
  },

  plugins: [require("tailwind-scrollbar")({ nocompatible: true })],
};
